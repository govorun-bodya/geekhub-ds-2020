#!/usr/bin/env python
# coding: utf-8

# In[382]:


import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
get_ipython().run_line_magic('config', "InlineBackend.figure_format = 'png'")
df1 = pd.read_csv('Seasons_Stats.csv')
df2 = pd.read_csv('player_data.csv')
df3 = pd.read_csv('Players.csv')


# 1) Удаляем пустые колонки, и колонки с ненужными данными которые видно сразу

# In[383]:


del df1['blanl'], df1['blank2'], df1['Player'], df1['Tm'], df1['Unnamed: 0']


# 2) Удаляем колонки значения в которых зависимы и выводятся используя другие колонки (df1['3P%']=df1['3PA']/df1['3P'])

# In[384]:


del df1['3P%'], df1['2P%'], df1['WS'], df1['BPM'], df1['FG%'], df1['FT%'], df1['TRB%'], df1['TRB']


# 3) Анализируем heatmap и удаляем колонки с похожими данными

# In[385]:


sns.set(rc={'figure.figsize':(15,15)})
sns.heatmap(df1.corr())
del df1['TS%'], df1['FG'], df1['3P'], df1['2P'], df1['FT']


# 4) Преобразовываем неудобные данные

# In[386]:


df1['Year']-=df1['Year'].min()


# Не использую One hot encoding, так как получилось бы слишком много колонок (24), а так всего 7

# In[387]:


df1['Pos-F'] = (~df1['Pos'].isin(['SG','G','PG','C','SG-PG','PG-SG'])).astype(int)
df1['Pos-G'] = df1['Pos'].isin(['G-F', 'SG','G''PG', 'F-G','SF-SG','SG-SF','SG-PG','SF-PG','PG-SG','PG-SF','SG-PF']).astype(int)
df1['Pos-C'] = df1['Pos'].isin(['F-C','C','C-F','PF-C','C-PF','C-SF']).astype(int)
df1['Pos-SG'] = df1['Pos'].isin(['SG','SF-SG','SG-SF','SG-PG','PG-SG', 'SG-PF']).astype(int)
df1['Pos-SF'] = df1['Pos'].isin(['SF','SF-SG','SG-SF','PF-SF','SF-PF','SF-PG','C-SF','PG-SF']).astype(int)
df1['Pos-PF'] = df1['Pos'].isin(['PF','PF-C','C-PF','PF-SF','SF-PF','SG-PF']).astype(int)
df1['Pos-PG'] = df1['Pos'].isin(['PG','SG-PG','SF-PG','PG-SG','PG-SF']).astype(int)
del df1['Pos']


# 5) Заполняем пропуски

# In[388]:


df1 = df1.fillna(df1.mean())

