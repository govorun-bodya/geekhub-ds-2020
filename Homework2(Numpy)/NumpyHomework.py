import numpy as np
url = "https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data"
iris = np.genfromtxt(url, delimiter=',', dtype='object')
names = ('sepallength', 'sepalwidth', 'petallength', 'petalwidth', 'species')

#----1,2----#
iris =np.delete(iris,4,1).astype(np.double)
#----3----#
print(np.mean(iris, 0)[0])
print(np.median(iris, 0)[0])
print(np.std(iris, 0)[0])

#----4----#
mask = np.zeros_like(iris.flatten())
mask[0:20] = 1
np.random.shuffle(mask)
mask = np.reshape(mask, iris.shape).astype('bool')
iris[mask] = np.nan
print("#4","\n",iris)
print("")

#----5----#
print("#5","\n", np.argwhere(np.isnan(iris[:, 0]))[:,0],"\n")

#----6----#
iris = iris[(iris[:,0]<5.)&(iris[:, 2]>1.5)]

#----7----#
iris = np.nan_to_num(iris)

#----8----#
arr = np.array(np.unique(iris, return_counts=True))
print("#8")
for i in range(arr[0].shape[0]):
  print(arr[0][i], " - ", int(arr[1][i]))

#----9----#
iris1, iris2 = np.vsplit(iris, 2)

#----10----#
iris1 = np.sort(iris1, 0)
iris2 = -np.sort(-iris2, 0)

#----11----#
iris = np.vstack((iris1, iris2))

#----12----#
arr = np.array(np.unique(iris, return_counts=True))
print("\n","#12","\n", arr[0][np.argmax(arr[1])])

#----13----#
def someFunc(arr):
  m1 = arr < np.mean(arr)
  m2 = arr > np.mean(arr)
  arr[m1]*=2
  arr[m2]/=4
  return arr
iris[:, 2] = someFunc(iris[:, 2])
print(iris)
