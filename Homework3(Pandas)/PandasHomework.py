import numpy as np
import pandas as pd

data = pd.read_csv('adult.data.csv')
data.head(50000)


#1


print("Males: ", data[data['sex']=="Male"].shape[0])
print("Females: ", data[data['sex']=="Female"].shape[0])


#2


print(data[data['sex']=="Female"]['age'].mean())


#3


print(data['native-country'].value_counts(normalize=True)['Germany']*100 , "%")


#4,5


df1 = data[data['salary']  == '<=50K']
df2 = data[data['salary']  == '>50K']
df2['age'].fillna(df2['age'].median, inplace=True)

print('Less than 50K: (mean, std) ', df1['age'].mean(), df1['age'].std())
print('More than 50K: (mean, std) ', df2['age'].mean(), df2['age'].std())

#6

assumption = True
for i in df1['education']:
    if i not in {'Bachelors','Prof-school','Assoc-acdm','Assoc-acdim','Assoc-voc','Masters','Doctorate'}:
        assumption = False
        break
print(assumption)

#7

print(data[data['sex']=='Male'].groupby('race')['age'].describe()['max'])
print(data[data['sex']=='Female'].groupby('race')['age'].describe()['max'])

#8

married = data[(data['marital-status'] == 'Married-civ-spouse') | (data['marital-status'] == 'Married-spouse-absent') | (data['marital-status'] == 'Married-AF-spouse')]
print("Married + high salary: ", married[married['salary'] == '>50K'].shape[0]/married[married['salary'] == '<=50K'].shape[0]*100, "%")
print("Non married + high salary: ", data[~data.isin(married)].dropna()[data[~data.isin(married)].dropna()['salary'] == '>50K'].shape[0]/data[~data.isin(married)].dropna()[data[~data.isin(married)].dropna()['salary'] == '<=50K'].shape[0]*100, "%")

#9

print("Max hours per week: ", data['hours-per-week'].max())
arr = data[data['hours-per-week']== data['hours-per-week'].max()]
print("Amought of people who work hard: ", arr.shape[0])
print(arr[arr['salary'] == '>50K'].shape[0]/arr[arr['salary'] == '<=50K'].shape[0]*100, '%')

#10

print('Small salary')
print(data[data['salary'] == '<=50K'].groupby('native-country')['hours-per-week'].describe()['mean'])
print('Big salary')
print(data[data['salary'] == '>50K'].groupby('native-country')['hours-per-week'].describe()['mean'])

